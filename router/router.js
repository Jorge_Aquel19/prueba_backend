const express = require('express')
const app = express();

app.use('/personas', require('./personas.router'));

module.exports = app;