const express = require('express');
const router = express.Router();
const l_persona = require('../class/personas');
const { _200, _500 } = require('../config/response');

router.post('/', async (req, res) => {
    try {

        let result = await l_persona.guardarPersona(req.body);
        res.status(200).send(_200(result.data, result.msj, result.code));
    } catch (error) {
        res.status(500).send(_500(error, result.msj));
    }
});

router.get('/', async (req, res) => {
    try {
        let result = await l_persona.buscarPersonas(req.body);
        res.status(200).send(result);
    } catch (error) {
        res.status(200).send(result);
    }
});

module.exports = router;