const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors');
require('./config/environment');

const app = express();

app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


// parse application/json
app.use(bodyParser.json());

app.use(require('./router/router'));

const PORT = process.env.PORT;

app.listen(PORT, () => {
    console.log(`Listening port ${PORT}`);
})