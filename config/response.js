const _500 = (err, message2 = 'Estamos teniendo problemas en el servidor, comuniquese con el administrador', code = 500, ) => {
    return {
        message: process.env.DEPLOY === 'produc' ? message2 : err.message,
        code,
        message2
    };
}

const _200 = (result = [], message = 'succes execute', code = 200) => {
    return {
        message,
        code,
        data: result
    }
}

module.exports = {
    _200,
    _500
}