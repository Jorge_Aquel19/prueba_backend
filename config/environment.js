const fs = require('fs')

//  VARIABLES DE ENTORNO

//PUERTO
process.env.PORT = 8080;

//CONFIGURACIONES MYSQL
process.env.HOST = 'localhost';
process.env.USER = 'jorge';
process.env.PORT_MYSQL = 3306;
process.env.PASSWORD = 'developer';
process.env.DB = 'prueba';


//DEPLOY STATUS;
// dev = desarrollo <==> product = produccion 
process.env.DEPLOY = 'dev';

// SECRET JWT
// process.env.SECRET = fs.readFileSync('config/secret.txt');
