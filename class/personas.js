const mysql = require('./../config/mysql');
const moment = require('moment');

class Persona {

    constructor() { }

    async guardarPersona(data) {

        let respuesta = {
            msj: 'Persona guardada con exito..!',
            data: [],
            code: 200
        };

        try {

            data.fecha_nacimiento = moment(new Date(data.fecha_nacimiento)).format('YYYY-MM-DD');

            const sql = await mysql.insert(`INSERT INTO personas (nombre, apellidos, tipoPersona, genero, tipoIdentificacion, numeroIdentificacion, fecha_nacimiento) VALUES (?,?,?,?,?,?,?)`, [data.nombre, data.apellidos, data.tipo_persona, data.genero, data.tipo_identificacion, data.identificacion, data.fecha_nacimiento]);

            if (sql.affectedRows > 0) {

                for (const iter of data.correos) {
                    console.log('CORREOS', sql.insertId);

                    await mysql.insert(`INSERT INTO Correos (fk_personas, email) VALUES (?,?)`, [sql.insertId, iter]);
                }

                for (const iter of data.telefonos) {
                    console.log('TELEFONOS', sql.insertId);
                    await mysql.insert(`INSERT INTO Telefonos (fk_personas, tipoTelefono, numeroTelefono) VALUES (?,?,?)`, [sql.insertId, iter.tipo, iter.numero]);
                }
            }

        } catch (error) {
            console.log(error);
            respuesta.msj = 'Ha ocurrido un error, si el error persiste comuniquese con el administrador';
            respuesta.code = 500;
        }

        return respuesta;
    }

    async buscarPersonas() {

        try {
            return await mysql.get(`SELECT CONCAT(nombre, ' ', apellidos) as nombre, numeroIdentificacion, genero`);
        } catch (error) {

        }
    }
}

module.exports = new Persona();